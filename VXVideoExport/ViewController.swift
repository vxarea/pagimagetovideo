//
//  ViewController.swift
//  VXVideoExport
//
//  Created by zhangxin on 2023/7/4.
//

import UIKit
import CoreVideo
import libpag

enum ConvertType {
    //使用pag的方法，边播放边保存祯缓存，播放结束后把祯缓存转成图片再合成视频
    case usePagSnapshot
    //使用自定义的截屏方法，边播边截屏，播放结束后把图片合成视频
    case useCustomerSnapshot
}
class ViewController: UIViewController, PAGViewListener {
    /**
     测试控制，用于确定当前使用哪种方式截屏转视频
     */
    public var convertType: ConvertType = .usePagSnapshot
    
    var timer: Timer?
    let fps = 24
    /**
     截屏间隔，1s钟24帧
     */
    var timeS = 1.0 / 24
   
    private var pixBufferArray: [Any] = []
    private var templeImageArray: [UIImage] = []
    
    var index = 0
    var count = 0
    ///pag动画
    lazy var pagImageView : PAGView = {
        let tView = PAGView()
        tView.frame = self.view.bounds
        let pagPath = Bundle.main.path(forResource: "lacunch_animation", ofType: "pag")
        guard let pagFile = PAGFile.load(pagPath) else {
            return tView
        }
        if pagFile.numTexts() > 0 {
            guard  let textData = pagFile.getTextData(0) else {
                return tView
            }
            textData.text = "hehe hello"
            pagFile.replaceText(0, data: textData)
        }
        if pagFile.numTexts() > 1 {
            guard  let textData = pagFile.getTextData(1) else {
                return tView
            }
            textData.text = "hahah, i am title 2"
            pagFile.replaceText(1, data: textData)
        }
        tView.contentMode = .scaleAspectFill
        tView.setComposition(pagFile)
        tView.setRepeatCount(3)
        if !tView.isPlaying() {
            tView.flush()
        }
        //添加listerner监听
        tView.add(self)
        tView.play()
        
        return tView
    }()
    lazy var videoHandler: VXVidepHandler = {
        let templePath = EditorFileManager.shared.createFolder(folderName: kvideoFolderName, filePath: kbaseFilePath)
        let th = VXVidepHandler(path: templePath, fileName: "VXVideoOut", videoSize: self.pagImageView.frame.size, fps: self.fps) { resultPath in
            guard let resultPath = resultPath else {
                return
            }
            print("开始合成背景音乐")
            let startDate = Date()
            let musicPath = Bundle.main.path(forResource: "StoryLineOne", ofType: "mp3")
            ImagesToVideoManager.mergerBackMusic(videoPath: resultPath ?? nil, backMusicPath: musicPath) { completedResult, videoFullPath, videoLocalIdentifier in
                let timeOut = Date().timeIntervalSince(startDate)
                if completedResult == .saveSuccess {
                    print("视频保存保存成功")
                } else if completedResult == .isNotPermissions {
                    print("相册权限未打开")
                } else {
                    print("保存失败")
                }
                print("视频转换结束，总共耗时：" + "\(timeOut)")
            }
        }
        return th!
    }()
    
    
    
    var pagSurface: PAGSurface?
    var pagPlayer: PAGPlayer = PAGPlayer()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        #warning("测试代码，记得删除")
        //HandlerVideo.sharedInstance().cusBegin(EditorFileManager.shared.createFolder(folderName: keffcttyleFolderName, filePath: kbaseFilePath), fileName: "testVideo", videoSize: self.pagImageView.frame.size, fps: 24)
        
      //  self.view.addSubview(self.pagImageView)
        
//         #warning ("测试代码，记得删除")
//        let musicPath = Bundle.main.path(forResource: "StoryLineOne", ofType: "mp3")
//        let tool = AudioTool()
////        tool.mergeAVAsset(withSourceURLs: [musicPath!, musicPath!]) { urlstr in
////            print("ceshishishis")
////        }
//
//        tool.combinationAudios(withAudioPath: [musicPath!, musicPath!], audioFullPath: EditorFileManager.shared.createFolder(folderName: kaudioFolderName, filePath: kbaseFilePath) + "/mixVideo.mp4") { tstate, urlstr in
//            print("ceshishishis")
//        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            self.videoHandler.testCu()
        })
        return;
       // VXVideoTool.sharedInstance().customerTest(templeArray)
        //self.videoHandler.endWriter()
        
    }
    private func getPixcelBuffer(value: Double) -> CVPixelBuffer? {
        pagPlayer.setProgress(value)
        pagPlayer.flush()
        let buffer = self.pagSurface?.getCVPixelBuffer()
        return buffer?.takeUnretainedValue()
    }
    
    
    private func releaseTimer() {
        if self.convertType == .useCustomerSnapshot {
            UIGraphicsEndImageContext()
        }
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
            return
        }
    }
    
    /*
     使用oc方法调用PAG提供的祯截图
     */
    private func userPagMethodScreenShot() {
        #warning("测试代码，记得删除")
        print("定时器时间:\(timeS)")
        
        self.timer = Timer.scheduledTimer(withTimeInterval: timeS, repeats: true) { [weak self] t_time in
            guard let self = self else {
                return
            }
            print("正在截图.......")
            //VXVideoTool.sharedInstance().cusScreenshot(self.pagImageView)
            self.videoHandler.snapShotView(self.pagImageView)
        }
        RunLoop.current.add(timer!, forMode: .common)
        timer?.fire()
    }
    
    
    
    //MARK: - pag listener
    func onAnimationStart(_ pagView: PAGView!) {
        print("动效开始执行")
        /*
        if self.convertType == .useCustomerSnapshot {
            //动效开始，开始截图,前几祯不要，是黑屏(动效还没出现在屏幕上)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.getPagSnapImageToArray(self.pagImageView)
            })
        } else if self.convertType == .usePagSnapshot {
            //动效开始，使用pag提供的祯截图
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.userPagMethodScreenShot()
            })
        }
         */
    }
    func onAnimationCancel(_ pagView: PAGView!) {
        print("动效取消")
    }
    func onAnimationRepeat(_ pagView: PAGView!) {
        print("重复执行")
    }
    func onAnimationEnd(_ pagView: PAGView!) {
        //tips: repeat为0的时候不会回调
        print("动效结束")
        print("开始转图片，请耐心等待，大约50s后会输出结果")
        self.releaseTimer()
        if self.convertType == .useCustomerSnapshot {
            //播放结束，开始把截图转成视频
            self.covertImageToVideo()
        } else if self.convertType == .usePagSnapshot {
            VXVideoTool.sharedInstance().startDate = Date()
            //VXVideoTool.sharedInstance().convertBufferToVideo()
            #warning("测试代码，记得删除")
            // HandlerVideo.sharedInstance().endWriter { state in
            //     print("测试代码，保存视频结束")
            // }
            //调用结束方法，停止视频祯写入
            self.videoHandler.endWriter()
        }
    }


}

//自定义边播放边自定义截图
extension ViewController {
    /**
     边播边截屏
     - view：需要截图的目标视图
     */
    func getPagSnapImageToArray(_ targetView: UIView) {
        templeImageArray.removeAll()
        //UIGraphicsBeginImageContextWithOptions(targetView.bounds.size, false, UIScreen.main.scale)
        timer = Timer.scheduledTimer(withTimeInterval: timeS, repeats: true) {[weak self] (templeTimer) in
            /*
            view.drawHierarchy(in: rect, afterScreenUpdates: false)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            if let imageT = image {
                self?.templeImageArray.append(imageT)
            }
             */
           // self?.index += 1
            self?.customerSnapShotView(targetView)
        }
        RunLoop.current.add(timer!, forMode: .common)
        timer?.fire()
    }
    
    
    private func customerSnapShotView(_ targetView: UIView) {
        /*
        autoreleasepool {
            UIGraphicsBeginImageContextWithOptions(targetView.bounds.size, false, UIScreen.main.scale)
            targetView.drawHierarchy(in: targetView.bounds, afterScreenUpdates: false)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            if let imageT = image {
                self.templeImageArray.append(imageT)
               // count += 1
               // EditorFileManager.shared.saveImageToFileFolder(image: imageT, imageName: "\(self.index)", folderName: kfilterFolderName)
            }
        }
         */
        UIGraphicsBeginImageContextWithOptions(targetView.bounds.size, false, UIScreen.main.scale)
        targetView.drawHierarchy(in: targetView.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let imageT = image {
            self.templeImageArray.append(imageT)
           // count += 1
           // EditorFileManager.shared.saveImageToFileFolder(image: imageT, imageName: "\(self.index)", folderName: kfilterFolderName)
        }
        
    }
    
    /**
     把截图转成视频
     */
    public func covertImageToVideo() {
        print("总的图片图片:\(self.count)")
        let startDate = Date()
        //背景音乐路径
       // let musicPath = Bundle.main.path(forResource: "StoryLineOne", ofType: "mp3")
        
        ImagesToVideoManager.composesVideoAndSaveToAlbumFullPath(templeImageArray, fps: Int32(fps), backMusicPath: nil, isSaveAlbum: false) { completedProgress in
            
        } completed: { completedResult, videoFullPath, videoLocalIdentifier in
            //这里删除的时候会crash
            //self?.templeImageArray.removeAll()
            let timeOut = Date().timeIntervalSince(startDate)
            if completedResult == .saveSuccess {
                print("视频保存保存成功")
            } else if completedResult == .isNotPermissions {
                print("相册权限未打开")
            } else {
                print("保存失败")
            }
            print("视频转换结束，总共耗时：" + "\(timeOut)")
        }
        
        /*
        ImagesToVideoManager.fileComposesVideoAndSaveToAlbumFullPath(self.count, fps: Int32(fps), backMusicPath: nil) { completedProgress in
            
        } completed: { completedResult, videoFullPath, videoLocalIdentifier in
            //这里删除的时候会crash
            //self?.templeImageArray.removeAll()
            let timeOut = Date().timeIntervalSince(startDate)
            if completedResult == .saveSuccess {
                print("视频保存保存成功")
            } else if completedResult == .isNotPermissions {
                print("相册权限未打开")
            } else {
                print("保存失败")
            }
            print("视频转换结束，总共耗时：" + "\(timeOut)")
        }
         */

    }
}

extension CVPixelBuffer {
    func getImage() -> UIImage? {
        CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags.readOnly)
        let ciImage: CIImage = CIImage(cvPixelBuffer: self)
        CVPixelBufferUnlockBaseAddress(self, .readOnly)
        let temporaryContext: CIContext = CIContext(options: nil)
        guard let videoImage: CGImage = temporaryContext.createCGImage(ciImage, from:
                                                                        CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(self),
                                                                               height: CVPixelBufferGetHeight(self)))
        else {
            return nil
        }
        let uiImage: UIImage = UIImage(cgImage: videoImage)
        return uiImage
    }
}
