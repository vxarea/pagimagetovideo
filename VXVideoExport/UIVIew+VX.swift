//
//  UIVIew+VX.swift
//  VXVideoExport
//
//  Created by zhangxin on 2023/7/5.
//


extension UIView {
    // 截图
    func screenshot() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(self.frame.size, true, UIScreen.main.scale)
        if let contenxt = UIGraphicsGetCurrentContext()  {
            self.layer.render(in: contenxt )
        }
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? UIImage()
        
        
    }
}
