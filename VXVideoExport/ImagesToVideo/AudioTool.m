//
//  AudioTool.m
//  MaxVideo
//
//  Created by VS on 2018/7/2.
//  Copyright © 2018年 VS. All rights reserved.
//

#import "AudioTool.h"
#import <AVFoundation/AVFoundation.h>
#import "VXVideoExport-Swift.h"

@implementation AudioTool

/**
 合并多个音频
 */
- (void)combinationAudiosWithAudioPath:(NSArray<NSString *> *)subsectionPaths
                        completedBlock:(CompFinalCompletedBlock)completedBlock {
    if (!subsectionPaths || subsectionPaths.count == 0) {
        NSLog(@"No such SubsectionNames");
        completedBlock(NO, @"合并失败", @"");
        return;
    }
     NSDictionary *optDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    //获取音轨
    AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    __block CMTime beginTime = kCMTimeZero;
    __block NSError *error = nil;
    [subsectionPaths enumerateObjectsUsingBlock:^(NSString *audioPath, NSUInteger idx, BOOL * _Nonnull stop) {
        AVAsset *audioAsset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:audioPath] options:optDict];
        NSArray *tracks = [audioAsset tracksWithMediaType:AVMediaTypeAudio];
        if (tracks <= 0) {
            *stop = YES;
            completedBlock(NO, @"合并失败", @"");
            return;
        }
        
        BOOL success = [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration) ofTrack:tracks.firstObject atTime:beginTime error:&error];
        if (!success) {
            *stop = YES;
            completedBlock(NO, error.localizedDescription, @"");
            return;
        }
        
        beginTime = CMTimeAdd(beginTime, audioAsset.duration);
    }];
    
    //tips: AVAssetExportPresetAppleM4A格式要和保存的格式一致
    AVAssetExportSession* assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetAppleM4A];
    //tips:自定义传入的路径会保存失败
    // assetExport.outputURL = [NSURL fileURLWithPath:audioFullPath];
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd-HH:mm:ss-SSS"];
    NSString * timeFromDateStr = [formater stringFromDate:[NSDate date]];
    //tips:要和AVAssetExportPresetAppleM4A格式一致，所以.mp4
    NSString *outPutFilePath = [NSHomeDirectory() stringByAppendingFormat:@"/tmp/sound-%@.mp4", timeFromDateStr];
    //音频文件输出
    assetExport.outputURL = [NSURL fileURLWithPath:outPutFilePath];
    assetExport.shouldOptimizeForNetworkUse = YES;
    assetExport.outputFileType = @"com.apple.m4a-audio";
    //导出
    [assetExport exportAsynchronouslyWithCompletionHandler:^{
        BOOL isSuccess = NO;
        NSString *msg = @"音频合并成功";
        switch (assetExport.status) {
            case AVAssetExportSessionStatusFailed:
                NSLog(@"AudioTool -> combinationError: %@", assetExport.error.localizedDescription);
                msg = @"音频合并失败";
                break;
            case AVAssetExportSessionStatusUnknown:
            case AVAssetExportSessionStatusCancelled:
                break;
            case AVAssetExportSessionStatusWaiting:
                break;
            case AVAssetExportSessionStatusExporting:
                break;
            case AVAssetExportSessionStatusCompleted:
                isSuccess = YES;
                break;
        }
        if (completedBlock) {
            completedBlock(isSuccess, msg, outPutFilePath);
        }
    }];
}


/**
 合成音频：给没有背景音乐的视频添加背景音乐
 
 @param musicPath 背景音乐地址
 @param videoPath 视频地址
 @param savePath 保存视频地址
 @param successBlock 合成成功
 */

- (void)mergeVideoWithMusic:(NSString *)musicPath noBgMusicVideo:(NSString *)videoPath saveVideoPath:(NSString *)savePath success:(mergeVideoSuccessBlock)successBlock{
    // 声音来源
    __block NSURL *audioInputUrl = [NSURL fileURLWithPath:musicPath];
    // 视频来源
    NSURL *videoInputUrl = [NSURL fileURLWithPath:videoPath];
    
    
    // 添加合成路径
    NSURL *outputFileUrl = [NSURL fileURLWithPath:savePath];
    // 时间起点
    CMTime nextClistartTime = kCMTimeZero;
    // 创建可变的音视频组合
    AVMutableComposition *comosition = [AVMutableComposition composition];
    
    
    // 视频采集
    AVURLAsset *videoAsset = [[AVURLAsset alloc] initWithURL:videoInputUrl options:nil];
    CMTime videoTime = videoAsset.duration;
    // 视频时间范围
    CMTimeRange videoTimeRange = CMTimeRangeMake(kCMTimeZero, videoTime);
    // 视频通道 枚举 kCMPersistentTrackID_Invalid = 0
    AVMutableCompositionTrack *videoTrack = [comosition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    // 视频采集通道
    AVAssetTrack *videoAssetTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
    //  把采集轨道数据加入到可变轨道之中
    [videoTrack insertTimeRange:videoTimeRange ofTrack:videoAssetTrack atTime:nextClistartTime error:nil];
    
    
    
    // 声音采集
    __block AVURLAsset *audioAsset = [[AVURLAsset alloc] initWithURL:audioInputUrl options:nil];
    // 音频的长度
    CMTime audioTime = [audioAsset duration];
    //判断是否需要拼接音频（音频比视频短的时候)
    NSLock *cusLock = [[NSLock alloc] init];
    float audioDurationSeconds = CMTimeGetSeconds(audioTime);
    float videoDurationSeconds = CMTimeGetSeconds(videoTime);
    if ((audioDurationSeconds < videoDurationSeconds) && (audioDurationSeconds > 0)) {
        NSInteger tvalue = ceil(videoDurationSeconds / audioDurationSeconds);
        NSMutableArray *urls = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < tvalue; i ++) {
            [urls addObject: musicPath];
        }
        [cusLock lock];
        [self combinationAudiosWithAudioPath:urls completedBlock:^(BOOL success, NSString *errorMsg, NSString *templeMusicPath) {
            audioInputUrl = [NSURL fileURLWithPath:templeMusicPath];
            audioAsset = [[AVURLAsset alloc] initWithURL:audioInputUrl options:nil];
            [cusLock unlock];
        }];
    }
    
    //音频时长和视频时长相等
    CMTimeRange audioTimeRange = videoTimeRange;
    // 音频通道
    AVMutableCompositionTrack *audioTrack = [comosition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    // 音频采集通道
    AVAssetTrack *audioAssetTrack = [[audioAsset tracksWithMediaType:AVMediaTypeAudio] firstObject];
    // 加入合成轨道之中
    [audioTrack insertTimeRange:audioTimeRange ofTrack:audioAssetTrack atTime:nextClistartTime error:nil];
    
    // 创建一个输出
    AVAssetExportSession *assetExport = [[AVAssetExportSession alloc] initWithAsset:comosition presetName:AVAssetExportPresetHighestQuality];
    // 输出类型
    assetExport.outputFileType = AVFileTypeMPEG4;
    // 输出地址
    assetExport.outputURL = outputFileUrl;
    // 优化
    assetExport.shouldOptimizeForNetworkUse = YES;
    
    
    /**
     淡入淡出设置, 大于10s的视频才给音乐淡入淡出(前后各5s）
     */
    
    if (videoDurationSeconds > 10.0) {
        //初始化音频混合器
        AVMutableAudioMix *exportAudioMix = [AVMutableAudioMix audioMix];
        //获取混合后的音轨
        AVAssetTrack *mixCompositionTrack = [[comosition tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
        //初始化音频混合器导出配置参数
        AVMutableAudioMixInputParameters *exportAudioMixInputParameters = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:mixCompositionTrack];

        //初始化淡入淡出持续时间  这个可以根据项目需求来设置这里前5秒淡入音量从0到1.0，最后5秒淡出音量从1.0到0 大家可以根据自己需求修改
        CMTime continueTime = CMTimeMakeWithSeconds(5, 1);
        //设置前5秒音乐淡入
        [exportAudioMixInputParameters setVolumeRampFromStartVolume:0 toEndVolume:1.0 timeRange:CMTimeRangeMake(CMTimeMakeWithSeconds(0, 1), continueTime)];
        //计算开始淡出的时间
        CMTime fadeOutStartTime = CMTimeSubtract(videoAsset.duration, continueTime);
        //设置最后5秒音乐淡出
        [exportAudioMixInputParameters setVolumeRampFromStartVolume:1.0 toEndVolume:0 timeRange:CMTimeRangeMake(fadeOutStartTime, continueTime)];
        //设置音频混合器参数
        NSArray *audioMixParameters = @[exportAudioMixInputParameters];
        exportAudioMix.inputParameters = audioMixParameters;
        //添加到导出配置中
        assetExport.audioMix = exportAudioMix;
    }
    
    // 合成完毕
    [assetExport exportAsynchronouslyWithCompletionHandler:^{
        // 回到主线程
        dispatch_async(dispatch_get_main_queue(), ^{
            
            successBlock(savePath);
            
        });
    }];
}


@end
