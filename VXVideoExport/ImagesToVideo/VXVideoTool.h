//
//  VXVideoTool.h
//  VXVideoExport
//
//  Created by zhangxin on 2023/7/5.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>
#import <UIKit/UIKit.h>
#import <libpag/PAGView.h>

NS_ASSUME_NONNULL_BEGIN

@interface VXVideoTool : NSObject
@property (nonatomic, copy) NSMutableArray *pixBufferArray;
@property (nonatomic, strong) NSDate *startDate;
+ (instancetype)sharedInstance;
/**
 边播放边保存buff，播放完毕后转图片
*/
- (void)cusScreenshot: (PAGView *)pagView;
/**
 传入buffer转成视频
 */
- (void)convertBufferToVideo;

- (void)customerTest: (NSArray *)pixArray;
@end

NS_ASSUME_NONNULL_END
