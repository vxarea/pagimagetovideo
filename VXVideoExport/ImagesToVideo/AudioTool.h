//
//  AudioTool.h
//  MaxVideo
//
//  Created by VS on 2018/7/2.
//  Copyright © 2018年 VS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CompFinalCompletedBlock)(BOOL success, NSString *errorMsg, NSString *outputPathStr);
typedef void(^mergeVideoSuccessBlock)(NSString *resultPath);
@interface AudioTool : NSObject

/**
 音频合并

 @param subsectionPaths 合并路径数组
 @param audioFullPath 导出路径（.m4a格式）
 @param completedBlock 完成回调
 */
- (void) combinationAudiosWithAudioPath:(NSArray<NSString *> *)subsectionPaths
                        completedBlock:(CompFinalCompletedBlock)completedBlock;


/**
 没有背景音乐的视频添加背景音乐
 
 @param musicPath 背景音乐地址
 @param videoPath 视频地址
 @param savePath 保存视频地址
 @param successBlock 合成成功
 */

- (void)mergeVideoWithMusic:(NSString *)musicPath noBgMusicVideo:(NSString *)videoPath saveVideoPath:(NSString *)savePath success:(mergeVideoSuccessBlock)successBlock;
@end
