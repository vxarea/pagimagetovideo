//
//  VXCameraSessionManager.swift
//  vx
//
//  Created by qiaoming on 2020/4/1.
//  Copyright © 2020 MISSU LIMITED. All rights reserved.
//
import UIKit
import AVFoundation
import Photos

class VXCameraSessionManager {

    //相机session
    private let session = AVCaptureSession()
    let previewLayer: AVCaptureVideoPreviewLayer
    private let stillImageOutput = AVCaptureStillImageOutput()
    
    //前置or后置摄像头
    var isBackCamera:Bool! {
        didSet {
            toggleCaptureDevicePosition(isBack: isBackCamera)
        }
    }
    
   
    init() {
        stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        previewLayer = AVCaptureVideoPreviewLayer.init(session: session)
        print("layer:\(previewLayer)")
    }
    

    // MARK:- Class Method
    
    typealias finishCallback = (_ granted: Bool) -> ()
    
    //申请相机权限
    class func requestCameraAceess(entrance: String, completion: @escaping finishCallback) {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
            DispatchQueue.main.async {
                completion(granted)
            }
        }
    }
    
    //申请相册权限
    class func requestAlbumAccess(entrance: String, completion: @escaping finishCallback) {
        PHPhotoLibrary.requestAuthorization { (status) in
            DispatchQueue.main.async {
                if PHAuthorizationStatus.authorized == status {
                    completion(true)
                } else {
                    completion(false)
                }
            }
            let code = status == .authorized ? "1":"0"
        }
    }
    
    //获取相机授权状态
    class func catchCameraAuthorizationStatus() -> AVAuthorizationStatus{
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        return status
    }
    
    //获取相册授权状态
    class func catchAlbumAuthorizationStatus() -> PHAuthorizationStatus{
        let status = PHPhotoLibrary.authorizationStatus()
        return status
    }
    
    // MARK:- Instance Method
    
    func sessionStartRunning() {
        if !session.isRunning {
            if session.inputs.count == 0 {
                guard let captureInput = captureInputWithPosition(isBack: true) else { return }
                if session.canAddInput(captureInput) {
                    session.addInput(captureInput)
                }
            }
            
            if session.outputs.count == 0 {
                if session.canAddOutput(stillImageOutput) {
                    session.addOutput(stillImageOutput)
                }
            }
            
            if session.inputs.count > 0 {
                session.startRunning()
            }
        }
    }
    
    
    func sessionStopRunning() {
        if session.isRunning {
            session.stopRunning()
        }
    }
    
    func toggleCamera() {
        isBackCamera = !isBackCamera
    }
    
    //获取实时照片
    func capturePhoto(completion: @escaping (_ imageData: NSData?) -> Void) {
        let videoConnection: AVCaptureConnection? = stillImageOutput.connection(with: AVMediaType.video)
        if let tvideoConnection = videoConnection {
            if tvideoConnection.isEnabled, tvideoConnection.isActive, session.isRunning {
                stillImageOutput.captureStillImageAsynchronously(from: tvideoConnection) { (imageDataSampleBuffer, error) in
                    print("xxxerror = \(error)")
                    if error != nil {
                        completion(nil)
                    } else {
                        if let timageDataSampleBuffer = imageDataSampleBuffer {
                            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(timageDataSampleBuffer)
                            completion(imageData as NSData?)
                            print("xxxerror = \(imageData)")
                        }
                    }
                }
            }
        }
        
        completion(nil)
    }
    
    
    // MARK:- Private
    
    private func captureInputWithPosition(isBack: Bool) -> AVCaptureDeviceInput? {
        
        let position = isBack ? AVCaptureDevice.Position.back : AVCaptureDevice.Position.front
        
        let devices = AVCaptureDevice.devices()
        var device: AVCaptureDevice?
        
        for obj in devices {
            if obj.hasMediaType(AVMediaType.video), obj.position == position {
                device = obj
                break
            }
        }
        guard let myDevice = device else {
            return nil
        }
        
        var captureDeviceInput: AVCaptureDeviceInput?
        do {
            captureDeviceInput = try AVCaptureDeviceInput(device: myDevice)
        } catch {
            print("Couldn't init device Input:\(error)")
        }
        
        if let input = captureDeviceInput {
            return input
        }
        
        return nil
    }
    
    private func toggleCaptureDevicePosition(isBack: Bool) {
        session.beginConfiguration()
        
        for obj in session.inputs {
            session.removeInput(obj)
        }
        
        guard let captureInput = captureInputWithPosition(isBack: isBack) else { return }
        if session.canAddInput(captureInput) {
            session.addInput(captureInput)
        }
        
        session.commitConfiguration()
    }
    
    
    deinit {
        if session.isRunning {
            session.stopRunning()
        }
    }
}

