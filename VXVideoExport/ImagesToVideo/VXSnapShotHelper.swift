//
//  VXSnapShotHelper.swift
//  VXVideoExport
//
//  Created by zhangxin on 2023/7/7.
//

import UIKit

class VXSnapShotHelper: NSObject {
    typealias WriteCallBack = (_ images:[UIImage]) ->()
    class func writeImageToArray(_ time: Int,_ view: UIView, wiriteCallBack: WriteCallBack?) {
        var templeDataArray : [UIImage] = []
        var i = 1
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        let rect = view.bounds
        let timer = Timer.scheduledTimer(withTimeInterval: 1/24, repeats: true) { (templeTimer) in
            view.drawHierarchy(in: rect, afterScreenUpdates: false)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            if i < time * 24 {
                i = i + 1
                if let imageT = image {
                    templeDataArray.append(imageT)
                }
            } else {
                UIGraphicsEndImageContext()
                //屏幕截图完成，图片数量 = time / 截图频率
                templeTimer.invalidate()
                //templeTimer = nil
                wiriteCallBack?(templeDataArray)
            }
            
        }
    }
}
