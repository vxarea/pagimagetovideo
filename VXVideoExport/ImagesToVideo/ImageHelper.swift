//
//  ImageHelper.swift

//
//  Created by zhangxin on 2020/8/26.
//  Copyright © 2020 Zy. All rights reserved.
//

import UIKit
import Photos

let appNameString : String = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String ?? "VXVideoExport"

class ImageHelper: NSObject {
    //保存图片到相册
   class func savePhotoToAlbum(withImage image:UIImage,handler:@escaping (_ isSuccess:Bool,_ isPermissions:Bool,_ localIdentifier: String)->()) {
        var status = PHPhotoLibrary.authorizationStatus()
        if #available(iOS 14, *) {
            status = PHPhotoLibrary.authorizationStatus(for: .readWrite)
        }
        if status == .notDetermined {
            VXCameraSessionManager.requestAlbumAccess(entrance: "") { granted in
                if granted {
                    ImageHelper.savePhotoToAlbum(withImage: image, handler: handler)
                }
            }
            return
        } else if status == .denied || status == .restricted {
            handler(false,false,"")
            return
        }
        if #available(iOS 14, *) {
            if status == .limited {
                //部分权限（iOS14上）
                print("权限受限,只有部分权限")
                let createdAssets = ImageHelper.createAssetsWithImage(image)
                if createdAssets != nil {
                    //保存成功，但没有读取的权限
                    handler(true,true,createdAssets.1!)
                    return
                }
            }
        }
        if #available(iOS 14, *) {
            if status == .limited {
                //部分权限（iOS14上）
                print("权限受限,只有部分权限")
                let createdAssets = ImageHelper.createAssetsWithImage(image)
                if createdAssets != nil {
                    //保存成功，但没有读取的权限
                    handler(true,true,createdAssets.1!)
                    return
                }
            }
        }
        //拿到自定义的相册对象
        let collection = ImageHelper.createCollection()
        //获得相片
        let createdAssets = ImageHelper.createAssetsWithImage(image)
        if createdAssets == nil || collection == nil {
            //没有权限或只有部分的读取权限
            handler(false,true,"")
            return
        }
        do {
            try PHPhotoLibrary.shared().performChangesAndWait {
                PHAssetCollectionChangeRequest.init(for: collection!, assets: createdAssets.0!)
            }
        } catch  {
            handler(false,true,"")
            print("图片保存到相册失败")
            return
        }
       handler(true,true,createdAssets.1!)
        
        
    }
    
    private class func createCollection() -> PHAssetCollection?{
        if #available(iOS 14, *) {
            let statues = PHPhotoLibrary.authorizationStatus(for: .readWrite)
            if statues == .limited {
                //权限受限
                return nil
            }
        }
        let collections = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
        for i in 0..<collections.count {
            let collection = collections[i] as? PHAssetCollection
            if collection?.localizedTitle == appNameString {
                return collection!
            }
        }
        var createdCollectionId : String? = nil
        do {
            try PHPhotoLibrary.shared().performChangesAndWait {
                createdCollectionId = PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: appNameString).placeholderForCreatedAssetCollection.localIdentifier
            }
        } catch  {
            
        }
        if createdCollectionId == nil {
            return nil
        }
        if let result = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [createdCollectionId!], options: nil).firstObject {
            return result
        }
        return nil
    }
    
    private class func createAssetsWithImage(_ image:UIImage) -> (PHFetchResult<PHAsset>?,localIdentifier: String?){
        var createdAssetId : String? = nil
        do {
           try PHPhotoLibrary.shared().performChangesAndWait {
               createdAssetId = PHAssetChangeRequest.creationRequestForAsset(from: image).placeholderForCreatedAsset?.localIdentifier
           }
       } catch  {
          
       }
        if createdAssetId == nil {
            return (nil,nil)
        }
        return (PHAsset.fetchAssets(withLocalIdentifiers: [createdAssetId!], options: nil),createdAssetId)
    }
    
    
    private class func createdAssetsWithVideoUrl(_ videoUrl:URL) -> (PHFetchResult<PHAsset>?,localIdentifier: String?){
        var createdAssetId : String? = nil
        do {
           try PHPhotoLibrary.shared().performChangesAndWait {
            createdAssetId = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl)?.placeholderForCreatedAsset?.localIdentifier
           }
       } catch  {
          
       }
        if createdAssetId == nil {
            return (nil ,nil)
        }
        return (PHAsset.fetchAssets(withLocalIdentifiers: [createdAssetId!], options: nil),createdAssetId)
    }
        
    
    class func saveToVideoAlbumWithUrl(_ videoUrl:URL,handler:@escaping (_ isSuccess:Bool,_ isPermissions:Bool ,_ localIdentifier: String)->()) {
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .notDetermined {
            VXCameraSessionManager.requestAlbumAccess(entrance: "") { granted in
                if granted {
                    ImageHelper.saveToVideoAlbumWithUrl(videoUrl, handler: handler)
                }
            }
            return
        } else if status == .denied || status == .restricted {
            handler(false ,false ,"")
            return
        }
        
        if #available(iOS 14, *) {
            let phStatus = PHPhotoLibrary.authorizationStatus(for: .readWrite)
            if phStatus == .limited {
                //权限受限，保存到相册成功，只是没有读取权限，此时也返回true，默认保存成功
                let createdAssets = ImageHelper.createdAssetsWithVideoUrl(videoUrl)
                if createdAssets.0 == nil || createdAssets.1 == nil {
                    handler(false ,true ,"")
                    return
                }
                handler(true,true, createdAssets.1!)
                return
            }
        }
        
        //拿到自定义的相册对象
        let collection = ImageHelper.createCollection()
        //获得相片
        let createdAssets = ImageHelper.createdAssetsWithVideoUrl(videoUrl)
        if createdAssets.0 == nil || createdAssets.1 == nil || collection == nil {
            handler(false ,true ,"")
            return
        }
        do {
            try PHPhotoLibrary.shared().performChangesAndWait {
                PHAssetCollectionChangeRequest.init(for: collection!, assets: createdAssets.0!)
            }
        } catch  {
            handler(false ,true ,"")
            print("视频保存到相册失败")
            return
        }
        handler(true,true, createdAssets.1!)
        
        
    }
}
