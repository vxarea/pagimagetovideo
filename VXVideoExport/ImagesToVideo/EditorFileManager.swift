//
//  APLFileManager.swift
//  AhaPhotoLab
//
//  Created by 张新 on 2020/2/18.
//  Copyright © 2020 MISSU LIMITED. All rights reserved.
//

import UIKit

//视频资源基本路径
let kvideoFilePath : String = NSHomeDirectory() + "/Documents/EditorVideos"
//音频资源基本路径
let kbaseFilePath : String = NSHomeDirectory() + "/Documents/EditorAudios"
//视频 编辑文件夹名称
var kvideoFolderName : String = "Video"
//音频 编辑文件夹名称
var kaudioFolderName : String = "Audio"


@objcMembers class EditorFileManager: NSObject {

    private static let subsInstance = EditorFileManager()
    //私有化构造方法
    private override init() { }
    //提供静态访问方法
    @objc public static var shared: EditorFileManager {
        return self.subsInstance
    }
    //在oc中这样写才能被调用
    @objc public class func sharedInstance() -> EditorFileManager{
        return EditorFileManager.shared
    }
    
    @objc var audioPath: String!
    @objc var videoPath: String!
    
    //创建全部文件夹
    @objc func createProjectAllFolder() {
        let videoPath =   self.createFolder(folderName: kvideoFolderName, filePath: kvideoFilePath)
        print("视频文件夹路径:" + videoPath)
        self.videoPath = videoPath
        let audioPath =   self.createFolder(folderName: kaudioFolderName, filePath: kbaseFilePath)
        print("音频文件夹路径:" + audioPath)
        self.audioPath = audioPath
    }
    
    //创建文件夹（目录)
    @objc func createFolder(folderName:String,filePath:String) ->String {
         let fileManager: FileManager = FileManager.default
         let filePath = filePath + "/" + "\(folderName)"
         let exist = fileManager.fileExists(atPath: filePath)
         if (!exist) {
          try! fileManager.createDirectory(atPath: filePath,withIntermediateDirectories: true, attributes: nil)
          }
         return filePath
    }
    
    
    //创建文件夹（目录)
    @objc func copyFolder(fromFolder: String, toPath: String){
         let fileManager: FileManager = FileManager.default
         let exist = fileManager.fileExists(atPath: fromFolder)
         if (exist) {
            try? fileManager.copyItem(atPath: fromFolder, toPath: toPath)
        }
    }
    
    //删除文件夹 filePath：文件路径
    @objc func removefolder(filePath:String){
        DispatchQueue.global().async {
            let fileManager: FileManager = FileManager.default
            let filePath = filePath
            let exist = fileManager.fileExists(atPath: filePath)
            // 查看文件夹是否存在，如果存在就直接读取，不存在就直接反空
            if (exist) {
                try? fileManager.removeItem(atPath: filePath)
            }else{
              // 不存在
            }
        }
    }
    
    //删除文件 filePath：文件路径,folderName:文件名称
    @objc func removefile(folderName: String,filePath:String){
        DispatchQueue.global().async {
            let fileManager: FileManager = FileManager.default
            let filePath = filePath + "/" + "\(folderName)"
            let exist = fileManager.fileExists(atPath: filePath)
            if (exist) {
                //移除文件
                try? fileManager.removeItem(atPath: filePath)
            }
        }
    }
    //判断文件或文件夹是否存在
    @objc func judgeFileOrFolderExists(folderName:String,filePath:String) ->Bool {
        let filePath = filePath + "/" + "\(folderName)"
        let fileManager: FileManager = FileManager.default
        let exist = fileManager.fileExists(atPath: filePath)
        // 查看文件夹是否存在，如果存在就直接读取，不存在就直接反空
        if (exist) {
           return true
        }else{
          return false
        }
    }
    
    //读取沙盒文件(判断文件是否存在）
    //判断指定视频或图片文件是否存在
    @objc func readTargetVideoExist(fileName name:String,filePath:String) -> Bool {
       let path = filePath + "/" + name
        let fileM = FileManager.default
        let  exist = fileM.fileExists(atPath: path)
        if exist == true {
            //存在
            return true
        } else {
            //不存在
            return false
        }
    }
    
    //获取文件路径
    @objc func returnTargetFilePath(fileName name:String,filePath:String) -> String {
        let path = filePath + "/" + name
        let fileM = FileManager.default
        let  exist = fileM.fileExists(atPath: path)
        if exist == true {
            //存在
            print(name+"的路径是:"+path)
            return path
        } else {
            //不存在
            print( name + "不存在")
            return ""
        }
    }
    
    //重命名文件
    @objc func renameFileName(oldName oName:String,newName nName:String,filePath:String) {
        var result = false
        let fileM = FileManager.default
        result =  ((try? fileM.moveItem(atPath: filePath + "/" + oName, toPath: filePath + "/" + nName)) != nil)
        var str = "失败"
        if result == true {
            str = "成功"
        }
        print("修改"+oName+"名称"+str)
    }
    
    
}

extension EditorFileManager {
    
    @discardableResult
    @objc func saveDataToFileFolder(data: Data, dataName: String,folderName:String) -> String? {
        let templeFilePath = self.createFolder(folderName: folderName, filePath: kbaseFilePath)
        let filePath = URL(fileURLWithPath: templeFilePath).appendingPathComponent(dataName).path
        do {
            try data.write(to: URL.init(fileURLWithPath: filePath))
        } catch {
            print("保存图片到本地文件夹失败")
        }
        //返回完整路径
        return filePath
    }
    
    //根据路径返回数据
    @objc func getDataWithFileFullPath(dataName: String,folderName:String) -> Data? {
        let filePath = self.returnTargetFilePath(fileName: dataName, filePath: kbaseFilePath + "/" + "\(folderName)")
        let data = FileManager.default.contents(atPath: filePath)
        return data
    }
}

extension EditorFileManager {
    //存储照片到指定的路径下
    @objc func saveImageToFileFolder(image: UIImage, imageName: String,folderName:String) -> String? {
        let fileName = imageName + ".png"
        let templeFilePath = self.createFolder(folderName: folderName, filePath: kbaseFilePath)
        let filePath = URL(fileURLWithPath: templeFilePath).appendingPathComponent(fileName ).path
        let imageData = image.pngData() as NSData?
        imageData?.write(toFile: filePath, atomically: true)
        #if DEBUG
        print(filePath)
        #endif

        //返回完整路径
        return filePath
    }
    //根据路径返回图片
    @objc func getImageWithFileFullPath(imageName: String,folderName:String) -> UIImage? {
        var filePath =  self.returnTargetFilePath(fileName: imageName + ".png", filePath:  kbaseFilePath + "/" + "\(folderName)")
        if filePath.count == 0{
            filePath =  self.returnTargetFilePath(fileName: imageName + ".jpg", filePath:  kbaseFilePath + "/" + "\(folderName)")
        }
        var originImage: UIImage?
        let dataImage = NSData(contentsOfFile: filePath) as Data?
        if let dataImage = dataImage {
            originImage = UIImage(data: dataImage)
        }
        return originImage
    }
    //清除kfilterFolderName中全部文件（退出总编辑页面时)
    @objc func clearAllFilterFolder() {
       // self.removefile(folderName: kfilterFolderName, filePath: kbaseFilePath)
    }
}
