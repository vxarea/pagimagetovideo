//
//  ImagesToVideoManager.swift

//
//  Created by qiaoming on 2020/9/9.
//  Copyright © 2020 Zy. All rights reserved.
//

import UIKit

@objc enum ImagesToVideoErrorCode : Int {
    //合成失败
    case composesFail = 10000
    //相册权限获取失败
    case isNotPermissions
    //保存相册失败
    case saveFail
    //保存相册成功
    case saveSuccess
    //视频合成成功
    case composesSuccess
}

class ImagesToVideoManager: NSObject {
    
    /**  图片转视频并添加背景音乐:
        - frameImgs:图片总数
        - fps: 帧率 每秒显示多少张图片
        - backMusicPath: 背景音乐的路径
        - finalResultPath:视频和影片合成的结果的路径
        - isSaveAlbum: 生成的结果是否保存到相册
    */
    @objc class func composesVideoAndSaveToAlbumFullPath(_ frameImgs: Array<UIImage>,
                                                         fps: Int32,
                                                         backMusicPath: String?,
                                                         finalResultPath: String = EditorFileManager.shared.createFolder(folderName: kvideoFolderName, filePath: kbaseFilePath) + "/mixAudioAndVideo.mp4",
                                                         isSaveAlbum: Bool = false,
                                                        composesProgress: @escaping(( _ completedProgress: CGFloat)->Void),
                                                         completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String? ,_ videoLocalIdentifier: String)->Void)) {
        
        var tempPath = EditorFileManager.shared.createFolder(folderName: kvideoFolderName, filePath: kbaseFilePath)
        tempPath = tempPath + "/imagesToVideo.mp4"        
        print("---视频开始合成-----")
        HandlerVideo.sharedInstance()?.composesVideoFullPath(tempPath, frameImgs: frameImgs, fps: fps, progressImageBlock: { (progress) in
            composesProgress(progress)
        }, completedBlock: { (success) in
            if !success {
                print("-----图片转视频失败----")
                completed(.composesFail, nil, "")
                return
            }
            print("---图片合成视频成功,结果保存路径：")
            print(tempPath)
            if backMusicPath == nil {
                //没有音频，不需要合成
                if isSaveAlbum {
                    ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: tempPath)!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                        if !isPermissions  {
                            print("相册权限未打开")
                            completed(.isNotPermissions, tempPath, "")
                            return
                        }
                        if isSuccess {
                            print("保存到相册成功")
                            completed(.saveSuccess, tempPath, videoLocalIdentifier)
                        } else {
                            print("保存到相册失败")
                            completed(.saveFail, tempPath, "")
                        }
                    }
                } else {
                    completed(.saveSuccess, tempPath, "videoLocalIdentifier")
                }
                
            } else {
                //有背景音乐，开始合成
                print("---开始融合背景音乐")
                let audioT = AudioTool()
                audioT.mergeVideo(withMusic: backMusicPath!, noBgMusicVideo: tempPath, saveVideoPath: finalResultPath) { finalPath in
                    print("融合背景音乐成功,文件路径:" + (finalPath ?? ""))
                    if isSaveAlbum {
                        ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: (finalPath ?? ""))!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                            if !isPermissions  {
                                print("相册权限未打开")
                                completed(.isNotPermissions, finalPath, "")
                                return
                            }
                            if isSuccess {
                                print("保存到相册成功")
                                completed(.saveSuccess, finalPath, videoLocalIdentifier)
                            } else {
                                print("保存到相册失败")
                                completed(.saveFail, finalPath, "")
                            }
                        }
                    } else {
                        completed(.saveSuccess, finalPath, "videoLocalIdentifier")
                    }
                    
                }
            }
        })
    }
    
    
    /**  添加背景音乐:
        - videoPath: 视频所在路径
        - backMusicPath: 背景音乐的路径
        - finalResultPath:视频和影片合成的结果的路径(名称必须时唯一的不能重复)
        - isSaveAlbum: 生成的结果是否保存到相册
    */
    @objc class func mergerBackMusic(videoPath: String?,
                                     backMusicPath: String?,
                                     finalResultPath: String = EditorFileManager.shared.createFolder(folderName: kvideoFolderName, filePath: kbaseFilePath) + "/\(Date().timeIntervalSince1970).mp4",
                                     isSaveAlbum: Bool = false,
                                     completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String? ,_ videoLocalIdentifier: String)->Void)) {
        guard let videoPath = videoPath else {
            completed(.saveFail, videoPath, "")
            return
        }
        
        if backMusicPath == nil {
            //没有音频，不需要合成
            if isSaveAlbum {
                ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: videoPath)!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                    if !isPermissions  {
                        print("相册权限未打开")
                        completed(.isNotPermissions, videoPath, "")
                        return
                    }
                    if isSuccess {
                        print("保存到相册成功")
                        completed(.saveSuccess, videoPath, videoLocalIdentifier)
                    } else {
                        print("保存到相册失败")
                        completed(.saveFail, videoPath, "")
                    }
                }
            } else {
                completed(.saveSuccess, videoPath, "videoLocalIdentifier")
            }
            
        } else {
            //有背景音乐，开始合成
            print("---开始融合背景音乐")
            let audioT = AudioTool()
            audioT.mergeVideo(withMusic: backMusicPath!, noBgMusicVideo: videoPath, saveVideoPath: finalResultPath) { finalPath in
                print("融合背景音乐成功,文件路径:" + (finalPath ?? ""))
                if isSaveAlbum {
                    ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: (finalPath ?? ""))!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                        if !isPermissions  {
                            print("相册权限未打开")
                            completed(.isNotPermissions, finalPath, "")
                            return
                        }
                        if isSuccess {
                            print("保存到相册成功")
                            completed(.saveSuccess, finalPath, videoLocalIdentifier)
                        } else {
                            print("保存到相册失败")
                            completed(.saveFail, finalPath, "")
                        }
                    }
                } else {
                    completed(.saveSuccess, finalPath, "videoLocalIdentifier")
                }
                
            }
        }
    }
    
    
    /**  图片转视频并添加背景音乐:
        - imageCount:图片总数
        - fps: 帧率 每秒显示多少张图片
        - backMusicPath: 背景音乐的路径
        - finalResultPath:视频和影片合成的结果的路径
        - isSaveAlbum: 生成的结果是否保存到相册
    */
    @objc class func fileComposesVideoAndSaveToAlbumFullPath(_ imageCount: Int,
                                                         fps: Int32,
                                                         backMusicPath: String?,
                                                         finalResultPath: String = EditorFileManager.shared.createFolder(folderName: kvideoFolderName, filePath: kbaseFilePath) + "/mixAudioAndVideo.mp4",
                                                         isSaveAlbum: Bool = false,
                                                        composesProgress: @escaping(( _ completedProgress: CGFloat)->Void),
                                                         completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String? ,_ videoLocalIdentifier: String)->Void)) {
        
        var tempPath = EditorFileManager.shared.createFolder(folderName: kvideoFolderName, filePath: kbaseFilePath)
        tempPath = tempPath + "/imagesToVideo.mp4"
        print("---视频开始合成-----")
        HandlerVideo.sharedInstance()?.fileComposesVideoFullPath(tempPath, imageCount: imageCount, fps: fps, progressImageBlock: { progress in
            
        }, completedBlock: { success in
            if !success {
                print("-----图片转视频失败----")
                completed(.composesFail, nil, "")
                return
            }
            print("---图片合成视频成功,结果保存路径：")
            print(tempPath)
            if backMusicPath == nil {
                //没有音频，不需要合成
                if isSaveAlbum {
                    ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: tempPath)!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                        if !isPermissions  {
                            print("相册权限未打开")
                            completed(.isNotPermissions, tempPath, "")
                            return
                        }
                        if isSuccess {
                            print("保存到相册成功")
                            completed(.saveSuccess, tempPath, videoLocalIdentifier)
                        } else {
                            print("保存到相册失败")
                            completed(.saveFail, tempPath, "")
                        }
                    }
                } else {
                    completed(.saveSuccess, tempPath, "videoLocalIdentifier")
                }
                
            } else {
                //有背景音乐，开始合成
                print("---开始融合背景音乐")
                let audioT = AudioTool()
                audioT.mergeVideo(withMusic: backMusicPath!, noBgMusicVideo: tempPath, saveVideoPath: finalResultPath) { finalPath in
                    print("融合背景音乐成功,文件路径:" + (finalPath ?? ""))
                    if isSaveAlbum {
                        ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: (finalPath ?? ""))!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                            if !isPermissions  {
                                print("相册权限未打开")
                                completed(.isNotPermissions, finalPath, "")
                                return
                            }
                            if isSuccess {
                                print("保存到相册成功")
                                completed(.saveSuccess, finalPath, videoLocalIdentifier)
                            } else {
                                print("保存到相册失败")
                                completed(.saveFail, finalPath, "")
                            }
                        }
                    } else {
                        completed(.saveSuccess, finalPath, "videoLocalIdentifier")
                    }
                    
                }
            }
        })
    }
    
    /*
        buffers:pixbuffer数组
        fps: 帧率 每秒显示多少张图片
    */
    @objc class func vxComposesVideoAndSaveToAlbumFullPath(_ buffers: [Any], firstImage: UIImage, fps: Int32, isNeedWater: Bool,
        composesProgress: @escaping(( _ completedProgress: CGFloat)->Void),
        completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String? ,_ videoLocalIdentifier: String)->Void)) {
        var tempPath = EditorFileManager.shared.createFolder(folderName: kvideoFolderName, filePath: kbaseFilePath)
        tempPath = tempPath + "/imagesToVideo.mp4"
        print("---视频开始合成")
        HandlerVideo.sharedInstance().vxComposesVideoFullPath(tempPath, buffers: buffers, firstImage: firstImage, fps: fps) { progress in
            composesProgress(progress)
        } completedBlock: { success in
            print("---视频合成结束,保存路径：")
            print(tempPath)
            
            if !success {
                completed(.composesFail, nil, "")
                return
            }
            
            DispatchQueue.main.async {
                ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: tempPath)!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                    if !isPermissions  {
                        print("相册权限未打开")
                        completed(.isNotPermissions, tempPath, "")
                        return
                    }
                    if isSuccess {
                        print("保存到相册成功")
                        completed(.saveSuccess, tempPath, videoLocalIdentifier)
                    } else {
                        print("保存到相册失败")
                        completed(.saveFail, tempPath, "")
                    }
                }
            }
        }
    }
    
    class func combinationAddWaterVideosAndSaveToAlbumWithVideoPath(_ subsectionPath: String, durationTime: Double, isNeedAddWater: Bool = true, composesProgress: @escaping(( _ completedProgress: CGFloat)->Void), 
                                                            completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String? ,_ videoLocalIdentifier: String)->Void)) {
        
        if isNeedAddWater {
            //加水印
            let waterSubsectionPath = subsectionPath.replacingOccurrences(of: ".mp4", with: "water.mp4")
            HandlerVideo.sharedInstance()?.addWatermaskVideo(withInputVideoPath: subsectionPath, outputVideoFullPath: waterSubsectionPath, completedBlock: { (success, errorMsg, outputStr) in
                if success {
                    ImagesToVideoManager.combinationVideosAndSaveToAlbumWithVideoPath(waterSubsectionPath, durationTime: durationTime, composesProgress: composesProgress, completed: completed)
                } else {
                    completed(.composesFail, nil, "")
                }
            })
        } else {
            ImagesToVideoManager.combinationVideosAndSaveToAlbumWithVideoPath(subsectionPath, durationTime: durationTime, composesProgress: composesProgress, completed: completed)
        }
        
    }
    
   
    
    
    private class func combinationVideosAndSaveToAlbumWithVideoPath(_ subsectionPath: String, durationTime: Double, isNeedAddWater: Bool = true, composesProgress: @escaping(( _ completedProgress: CGFloat)->Void), 
                                                            completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String? ,_ videoLocalIdentifier: String)->Void)) {
        let fullPath = subsectionPath.replacingOccurrences(of: ".mp4", with: "full.mp4")
        
        var pathArray = [String]()
        for i in 1...Int(5 / durationTime + 0.5) {
            pathArray.append(subsectionPath)
        }
        pathArray.append(subsectionPath)
        print("---开始合成")
        HandlerVideo.sharedInstance()?.combinationVideos(withVideoPath: pathArray, videoFullPath: fullPath, isHavaAudio: false, progressBlock: { (progress) in
            composesProgress(progress)
        }, completedBlock: { (success, errorMsg, outputStr) in
            print("---合成结束")
            if !success {
                completed(.composesFail, nil, "")
                return
            }
            ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: fullPath)!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                if !isPermissions  {
                    print("相册权限未打开")
                    completed(.isNotPermissions, fullPath, "")
                    return
                }
                if isSuccess {
                    print("保存成功")
                    completed(.saveSuccess, fullPath, videoLocalIdentifier)
                } else {
                    print("保存失败")
                    completed(.saveFail, fullPath, "")
                }
            }
        })
    }
    
    class func composesVideoWithFullPath(_ tempPath: String,_ frameImgs: Array<UIImage>, _ fps: Int32, 
                                     composesProgress: @escaping(( _ completedProgress: CGFloat)->Void), 
                                     completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String?)->Void)) {      
        print("---开始合成")
        HandlerVideo.sharedInstance()?.composesVideoFullPath(tempPath, frameImgs: frameImgs, fps: fps, progressImageBlock: { (progress) in
            composesProgress(progress)
        }, completedBlock: { (success) in
            print("---合成结束")
            if !success {
                completed(.composesFail, nil)
                return
            } else {
                completed(.composesSuccess, tempPath)
            }
        })
    }
    
    class func composesVideoFullPath(_ frameImgs: Array<UIImage>, _ fps: Int32, 
                                     composesProgress: @escaping(( _ completedProgress: CGFloat)->Void), 
                                     completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String?)->Void)) {
        var tempPath = EditorFileManager.shared.createFolder(folderName: kvideoFolderName, filePath: kbaseFilePath)
        tempPath = tempPath + "/imagesToVideo.mp4"       
        print("---开始合成")
        HandlerVideo.sharedInstance()?.composesVideoFullPath(tempPath, frameImgs: frameImgs, fps: fps, progressImageBlock: { (progress) in
            composesProgress(progress)
        }, completedBlock: { (success) in
            print("---合成结束")
            if !success {
                completed(.composesFail, nil)
                return
            }
            completed(.composesSuccess, tempPath)
            
        })
    }
    
    class func addWaterVideoAndSaveToAlbumWithVideoPath(_ inputVideoPath: String, _ isNeedWater: Bool = true ,composesProgress: @escaping(( _ completedProgress: CGFloat)->Void), completed: @escaping(( _ completedResult: ImagesToVideoErrorCode, _ videoFullPath: String? ,_ videoLocalIdentifier: String)->Void)) {
        if !isNeedWater {
            let fullPath = inputVideoPath
            ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: fullPath)!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                if !isPermissions  {
                    print("相册权限未打开")
                    completed(.isNotPermissions, fullPath, "")
                    return
                }
                if isSuccess {
                    print("保存成功")
                    completed(.saveSuccess, fullPath, videoLocalIdentifier)
                } else {
                    print("保存失败")
                    completed(.saveFail, fullPath, "")
                }
            }
            return
        }
        
        var outputVideoPath = inputVideoPath.replacingOccurrences(of: ".mp4", with: "water.mp4")
        if inputVideoPath.hasSuffix(".mov") {
            outputVideoPath = inputVideoPath.replacingOccurrences(of: ".mov", with: "water.mov")
        }

        HandlerVideo.sharedInstance()?.addWatermaskVideo(withInputVideoPath: inputVideoPath, outputVideoFullPath: outputVideoPath, completedBlock: { (success, errorMsg, outputStr) in
//            if !success {
//                completed(.composesFail, nil, "")
//                return
//            }
            let fullPath = success ? outputVideoPath : inputVideoPath
            ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: fullPath)!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
                if !isPermissions  {
                    print("相册权限未打开")
                    completed(.isNotPermissions, fullPath, "")
                    return
                }
                if isSuccess {
                    print("保存成功")
                    completed(.saveSuccess, fullPath, videoLocalIdentifier)
                } else {
                    print("保存失败")
                    completed(.saveFail, fullPath, "")
                }
            }
        })
        
    }
    
    class func saveToVideoAlbumFullPath(_ videoFullPath: String,
                                        completed: @escaping(( _ completedResult: ImagesToVideoErrorCode)->Void)) {
        ImageHelper.saveToVideoAlbumWithUrl(URL.init(string: videoFullPath)!) { (isSuccess, isPermissions ,videoLocalIdentifier) in
            if !isPermissions  {
                print("相册权限未打开")
                completed(.isNotPermissions)
                return
            }
            if isSuccess {
                print("保存成功")
                completed(.saveSuccess)
            } else {
                print("保存成功")
                completed(.saveFail)
            }
        }
    }
    
    
    
    
}
