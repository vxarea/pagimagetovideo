//
//  VXVideoTool.m
//  VXVideoExport
//
//  Created by zhangxin on 2023/7/5.
//

#import "VXVideoTool.h"
#import "HandlerVideo.h"
#import "VXVideoExport-Swift.h"


@implementation VXVideoTool

+ (instancetype)sharedInstance {
    static VXVideoTool *_instance;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (NSMutableArray *)pixBufferArray {
    if (!_pixBufferArray) {
        _pixBufferArray = [[NSMutableArray alloc] init];
    }
    return _pixBufferArray;
}

/**
 缓存buffer
 */
- (void)cusScreenshot: (PAGView *)pagView {
    CVPixelBufferRef tbuff = [pagView makeSnapshot];
    //UIImage *image = [self convert:tbuff];
    if (tbuff != nil) {
        NSLog(@"test ===");
        //[self.pixBufferArray addObject:(__bridge id _Nonnull)(tbuff)];
        [[HandlerVideo sharedInstance] writerBuffer:tbuff];
         
    }
}
/**
 传入buffer转成视频
 */
- (void)convertBufferToVideo {
    UIImage *image;
    if (self.pixBufferArray.count > 0) {
        CVImageBufferRef tbuff =  (__bridge CVImageBufferRef)(self.pixBufferArray[0]);
        image = [self convert:tbuff];
    }
    NSLog(@"图片转换全部完成，开始转视频");
    NSDate *cVideDate = [NSDate date];
    __weak typeof(self) weakSelf = self;
    [ImagesToVideoManager vxComposesVideoAndSaveToAlbumFullPath: self.pixBufferArray firstImage: image fps: 24 isNeedWater: false composesProgress:^(CGFloat) {
            
    } completed:^(enum ImagesToVideoErrorCode, NSString * _Nullable videoPath, NSString * _Nonnull videoLocalIdentifier) {
        //[weakSelf.pixBufferArray removeAllObjects];
        NSInteger time1 = [[NSDate date] timeIntervalSinceDate: cVideDate];
        NSInteger time2 = [[NSDate date] timeIntervalSinceDate:self.startDate];
        NSLog([NSString stringWithFormat:@"视频保存结束,从图片结果到视频花费时间:%d", time1]);
        NSLog([NSString stringWithFormat:@"视频保存结束,整个过程总共花费时间:%d", time2]);
    }];
    
}

- (void)customerTest: (NSArray *)pixArray {
    UIImage *image;
    if (pixArray.count > 0) {
        CVImageBufferRef tbuff = CFBridgingRetain(self.pixBufferArray[0]);
        image = [self convert:tbuff];
    }
    NSLog(@"图片转换全部完成，开始转视频");
    NSDate *cVideDate = [NSDate date];
    __weak typeof(self) weakSelf = self;
    [ImagesToVideoManager vxComposesVideoAndSaveToAlbumFullPath: pixArray firstImage: image fps: 24 isNeedWater: false composesProgress:^(CGFloat) {
            
    } completed:^(enum ImagesToVideoErrorCode, NSString * _Nullable videoPath, NSString * _Nonnull videoLocalIdentifier) {
        //[weakSelf.pixBufferArray removeAllObjects];
        NSInteger time1 = [[NSDate date] timeIntervalSinceDate: cVideDate];
        NSInteger time2 = [[NSDate date] timeIntervalSinceDate:self.startDate];
        NSLog([NSString stringWithFormat:@"视频保存结束,从图片结果到视频花费时间:%d", time1]);
        NSLog([NSString stringWithFormat:@"视频保存结束,整个过程总共花费时间:%d", time2]);
    }];
}

/**
   CVPixelBufferRef to UIimage
 */
- (UIImage *)convert:(CVPixelBufferRef)pixelBuffer {
    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];

    CIContext *temporaryContext = [CIContext contextWithOptions:nil];
    CGImageRef videoImage = [temporaryContext
        createCGImage:ciImage
             fromRect:CGRectMake(0, 0, CVPixelBufferGetWidth(pixelBuffer), CVPixelBufferGetHeight(pixelBuffer))];

    UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
    CGImageRelease(videoImage);

    return uiImage;
}

@end
