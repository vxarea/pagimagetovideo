//
//  VXVidepHandler.h
//  VXVideoExport
//
//  Created by zhangxin on 2023/7/8.
//采用边播放边截取buffer写入bufferAdapter的方式，buffer不作中间存储，节省内存（不然内存会爆掉）

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <libpag/PAGView.h>

typedef void(^CompletedCallBack) (NSString *resultPath);
@interface VXVidepHandler : NSObject
@property (nonatomic, copy) CompletedCallBack completedBlock;
//初始化
- (instancetype)initWithPath:(NSString *)videoPath fileName: (NSString *)videoName videoSize: (CGSize)size fps: (NSInteger)fps completeCallBack: (CompletedCallBack)callback;
- (void)endWriter;
- (void)snapShotView: (PAGView *)pageView ;
- (void)writerBuffer: (CVPixelBufferRef )buffer;
- (void)testCu;
@end

