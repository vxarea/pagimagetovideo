//
//  VXVidepHandler.m
//  VXVideoExport
//
//  Created by zhangxin on 2023/7/8.
//

#import "VXVidepHandler.h"
#import "VXVideoExport-Swift.h"


@interface VXVidepHandler()

@property (nonatomic, strong) AVAssetWriter *videoWriter;
@property (nonatomic, assign) dispatch_queue_t dispatchQueue;
@property (nonatomic, strong) AVAssetWriterInput *writerInput;
@property (nonatomic, strong) AVAssetWriterInputPixelBufferAdaptor *adaptor;
@property (nonatomic, assign) NSInteger tFrame;
@property (nonatomic, assign) NSInteger fps;
@property (nonatomic, assign) CGSize videoSize;
@property (nonatomic, copy) NSString *resultPath;
@property (nonatomic, assign) CMTime frameTime;
@property (nonatomic, weak) NSTimer *timer;

@end

@implementation VXVidepHandler

- (void)dealloc {
    [self endWriter];
    [self.timer invalidate];
    self.timer = nil;
}


- (instancetype)initWithPath:(NSString *)videoPath fileName: (NSString *)videoName videoSize: (CGSize)size fps: (NSInteger)fps completeCallBack: (CompletedCallBack)callback {
    self = [super init];
    if (self) {
        //创建全部文件夹
        [[EditorFileManager shared] createProjectAllFolder];
        self.completedBlock = callback;
        self.tFrame = -1;
        self.fps = fps;
        self.videoSize = size;
        self.frameTime = CMTimeMake(1, fps);
        
        NSString *tempPath = [NSString stringWithFormat:@"%@/%@.mp4", videoPath, videoName];
        NSLog(@"视频保存地址：");
        NSLog(tempPath);
        
        self.resultPath = tempPath;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
            [[NSFileManager defaultManager] removeItemAtPath:tempPath error:nil];
        }
        NSError *error = nil;
        self.videoWriter = [[AVAssetWriter alloc] initWithURL:[NSURL fileURLWithPath:tempPath]
                                                               fileType:AVFileTypeQuickTimeMovie
                                                                  error:&error];
        NSParameterAssert(self.videoWriter);
        if(error)
            NSLog(@"error = %@", [error localizedDescription]);
        
        //获取原视频尺寸
        //tips:暂时使用外部传入的size
        //CGSize size = CGSizeMake(width, width * CGImageGetHeight(img.CGImage) / CGImageGetWidth(img.CGImage));
        
        NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:AVVideoCodecTypeH264, AVVideoCodecKey,
                                       [NSNumber numberWithInt:size.width], AVVideoWidthKey,
                                       [NSNumber numberWithInt:size.height], AVVideoHeightKey, nil];
        self.writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
        
        self.writerInput.expectsMediaDataInRealTime = true;
        
        
        NSDictionary *sourcePixelBufferAttributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:kCVPixelFormatType_32ARGB], kCVPixelBufferPixelFormatTypeKey, nil];
        
        self.adaptor = [AVAssetWriterInputPixelBufferAdaptor
                                                         assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.writerInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
        NSParameterAssert(self.writerInput);
        NSParameterAssert([self.videoWriter canAddInput:self.writerInput]);
        [self.videoWriter addInput:self.writerInput];
        [self.videoWriter startWriting];
        [self.videoWriter startSessionAtSourceTime:kCMTimeZero];
    }
    return  self;
}

/*
 
 */
- (void)testCu {
    NSString *pagPath = [[NSBundle mainBundle] pathForResource:@"lacunch_animation" ofType:@"pag"];
    PAGFile *pagFile = [PAGFile Load: pagPath];
    CGSize templeSize = pagFile.getBounds.size;
    PAGComposition *pagComposition = [PAGComposition Make:templeSize];
    PAGSurface *surface = [PAGSurface MakeOffscreen:templeSize];
    [pagComposition addLayer:pagFile];
    __block PAGPlayer *pagPlayer = [[PAGPlayer alloc] init];
    [pagPlayer setSurface:surface];
    [pagPlayer setComposition:pagComposition];
    
    //每s 24张截图
    CGFloat pagTime = pagFile.duration / 1000000.0;
    NSInteger totalCount = pagTime * 24;
    __block CGFloat timeOut = 1.0 / totalCount;
    __block NSInteger testCount = 0;
    __block CGFloat i = 0;
    __weak typeof(self) wself = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MAX(0, 0.5) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __weak NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timeOut repeats:true block:^(NSTimer * _Nonnull timer) {
            [pagPlayer setProgress:i];
            [pagPlayer flush];
            CVPixelBufferRef buffer = [surface getCVPixelBuffer] ;
            [wself writerBuffer: buffer];
            i = i + timeOut;
            if (i >= 1) {
                [timer invalidate];
                timer = nil;
                [wself endWriter];
            }
        }];
        self.timer = timer;
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        [timer fire];
    });
    
    
}


/**
 传入pageView开始截屏
 在swift中makeSnapshot获取到的对象无法转换操作，所以放到oc中
 */
- (void)snapShotView: (PAGView *)pageView {
    //tips：注意截屏要在异步线程，不然达不到设置的fps张数（根据手机性能不同，张数会不一样）
    __weak typeof (self) wself = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        CVPixelBufferRef buffer = [pageView makeSnapshot];
        [wself writerBuffer: buffer];
    });
   
}

- (void)writerBuffer: (CVPixelBufferRef )buffer {
    if ([self.writerInput isReadyForMoreMediaData]) {
        ++self.tFrame;
        if (buffer) {
            if (self.tFrame == 0) {
                [self.adaptor appendPixelBuffer: buffer withPresentationTime:kCMTimeZero];
            } else {
                CMTime lastTime = CMTimeMake(self.tFrame - 1, self.frameTime.timescale);
                CMTime pTime = CMTimeAdd(lastTime, self.frameTime);
                /*
                if(![self.adaptor appendPixelBuffer:buffer withPresentationTime:CMTimeMake(self.tFrame, self.fps)]) {
                    NSLog(@"FAIL");
                } else {
                   //CFRelease(buffer);
                }
                 */
                if(![self.adaptor appendPixelBuffer:buffer withPresentationTime: pTime]) {
                    NSLog(@"FAIL");
                } else {
                   //CFRelease(buffer);
                }
            }
           
        }
    }
}

/**
 停止
 */
- (void)endWriter {
    __weak typeof (self) wself = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [wself.writerInput markAsFinished];
        [wself.videoWriter finishWriting];
        printf("comp completed\n");
        if (wself.completedBlock) {
            wself.completedBlock(wself.resultPath);
        }
    });
    
   
}
/*
- (void)dealloc {
    if (self.writerInput) {
        [self.writerInput markAsFinished];
    }
    if (self.videoWriter){
        [self.videoWriter finishWriting];
    }
}
 */
@end
