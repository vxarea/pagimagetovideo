# PAGImageToVideo

pag动画效果截屏转视频


使用ConvertType为useCustomerSnapshot在iPhone14上，设置fps为24

15s的pag视频边播边截图，产生约356张图片

播放完成后356张图片转视频耗时：3s-5s之内，内存消耗峰值：101.8MB

test1: 视频转换结束，总共耗时：4.550366997718811

test2: 视频转换结束，总共耗时：3.6532130241394043




使用ConverType为usePagSnapshot，边截buffer边写入pixbufferadapter

45s的page视频，设置fps为24，产生截图（pixbuffer）1045张

在iPhone14上，保存视频（加背景音乐）耗时：0.041s

在iPhoneX上，保存视频（加背景音乐）耗时：1.89s


